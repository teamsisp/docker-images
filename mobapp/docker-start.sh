#!/bin/bash
chown -R switch:www-data /var/www/html  && chmod -R 775 /var/www/html/

cd $(readlink current)/src && composer install --no-scripts

exec "$@"
